import random

class HumanImitator:
    def _suggest_new_phrases(self,*args,**kwargs):
        raise NotImplementedError
    def suggest_new_phrases(self,*args,**kwargs):
        return self._suggest_new_phrases(*args,**kwargs)


class UserImitatorWithClassfication(HumanImitator):

    def __init__(self,
                 message_classifier,
                 suggester):

        self.message_classfier = message_classifier

        self.suggester = suggester

    def _suggest_new_phrases(self, dialog):

        categorized_dialog = self.message_classfier.categorize(dialog)
        suggested_texts = self.suggester.suggest(categorized_dialog)

        return suggested_texts

