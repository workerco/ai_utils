import json
import pandas as pd
import datetime
import itertools
import tqdm


def split_user_dialog_by_days(dialog, delta=None):

    dialog = pd.DataFrame(dialog)

    dialog['day_mark'] = dialog['date'].apply(lambda date: date - delta) \
        .apply(lambda date: (date.year, date.month, date.day))

    dialog = dialog.groupby('day_mark') \
        .apply(lambda day_data: day_data[['name', 'text', 'category', 'rank', 'date']].apply(dict, axis=1).tolist())

    day_dialogs = dialog.tolist()

    return day_dialogs

with open('configs/day_spliter.json','r') as file:
    config = json.load(file)

print(config)

### hardcode change of UTC+0 to American time
delta = datetime.timedelta(hours=5)
###

data = pd.read_pickle(config['load_path'])
day_dialogs = [split_user_dialog_by_days(dialog, delta) for dialog in tqdm.tqdm(data)]
day_dialogs = list(itertools.chain(*day_dialogs))

pd.to_pickle(day_dialogs,config['save_path'])