import pandas as pd
import json
import tqdm
import random
from collections import defaultdict
from pprint import pprint


categories = ['water', 'sleep', 'stress', 'workout', 'food', 'none', 'energy']


coach_context_questions = {'How many hours of shut eye did you get last night?\n- 8+ hrs\n- 7 hrs\n- 6 hrs\n- less than 5 hrs': 'sleep',
 'Describe your stress level today.\n- stress free\n- handled what was thrown my way well\n- overwhelmed, derailed my day\n- too much to handle, shut down.': 'stress',
 'How much water have you had today?\n- 64 oz for sure\n- more like 32 oz, but definitely hydrated\n- a few sips here and there but not enough\n- none! dehydrated for sure': 'water',
 'How is your food today?\n- on point\n- almost perfect\n- need to step it up\n- not good': 'food',
 'How active were you today?\n- Worked out (running, gym, hiking, sports)\n- Moderate exercise (walking, cycling, dancing)\n- Light activity (chores and errands, walking outside)\n- Not active at all, barely moved': 'workout',
 'How is the quality of your food intake today?\n- on point\n- almost perfect\n- need to step it up\n- not good': 'food'}

def create_train_string(dialog):

    coach_messages = [item['text'] for item in dialog if item['name'] == 'send']

    user_messages = [' '.join(item['text'].split()) for item in dialog if item['name'] == 'received']

    coach_context = []

    for message in coach_messages:
        if message in coach_context_questions:
            coach_context.append(coach_context_questions[message])

    coach_context = "Coach: " + ' '.join(coach_context) + " [SEP] "
    user_messages = " [SEP] ".join(user_messages)

    return coach_context + user_messages

def create_train_items(dialog):

    dialog.sort(key = lambda x: x['date'])

    dialog_date = dialog[0]['date']
    dialog_date = f"{dialog_date.year}_{dialog_date.month}_{dialog_date.day}"

    index_of_categorized = [idx for idx, item in enumerate(dialog) if item['category'] in categories]
    user_index = [idx for idx, item in enumerate(dialog) if item['name'] == 'received']


    for idx in index_of_categorized:

        split = dialog[:idx+1]

        string = create_train_string(split)

        string = string

        item = {
            "text" : string,
            "category": dialog[idx]['category'],
            "date": dialog_date
        }

        yield item

    for idx in user_index:

        split = dialog[:idx + 1]

        string = create_train_string(split)

        string = string

        item = {
            "text": string,
            "category": "Empty",
            "date": dialog_date
        }

        yield item




def cnt_categorized_messages(data):

    class_counter = defaultdict(int)

    for dialog in tqdm.tqdm(data):

        selected_classes = set()

        for item in dialog:
            selected_classes.add(item['category'])

        for class_name in selected_classes:
            class_counter[class_name] += 1

    return class_counter

random.seed(42)
train_test_ration = 0.1

over_sample_clasess = {
    "none": 2
}

class_counter = defaultdict(int)

collected_for_oversampling = defaultdict(list)

data = pd.read_pickle('dump/splitted_by_days.pkl')

print("distribution of classes before")
pprint(cnt_categorized_messages(data))

train_items = [item for dialog in tqdm.tqdm(data) for item in create_train_items(dialog)]

train_items = pd.DataFrame(train_items)

print("distribution of classes after")
print(train_items['category'].value_counts())

train_items.to_csv("dump/pretrain.csv")
