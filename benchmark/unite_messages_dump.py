import json
import os
import re
import pandas as pd
import pymongo

with open('configs/unite_dumps.config', 'r') as file:
    config = json.load(file)

paths = [path for path in os.listdir(config['load_dir']) \
 if re.compile(config['file_name_selector']).fullmatch(path)]

print(paths)

dumps = []

for path in paths:
    dump = pd.read_pickle(os.path.join(config['load_dir'],path))
    dump['dump_creation'] = path
    dumps.append(dump)

united = pd.concat(dumps)

print(united['category'].value_counts())

united = united.sort_values('dump_creation',ascending=False)
united = united.reset_index(drop=True)
united = united[~united['_id'].duplicated()]
united['send_date'] = united['date'].apply(lambda date : (date.year, date.month, date.day))


if config['fill_na']:
    united['category'] = united['category'].fillna('empty')

print(united['category'].value_counts())

united.to_pickle(config['save_path'])

