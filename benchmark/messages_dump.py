import json
import pymongo
import pandas as pd
import datetime
from pprint import pprint
from bson.objectid import ObjectId

today = datetime.datetime.today()

with open('configs/dump.config','r') as file:
    config = json.load(file)

client = pymongo.MongoClient(config['url'])
database = client["heroku_tf7m6qpv"]
collection = database["event_trackers"]

pipeline = [
    {"$match": {
            "name": {"$in" : ["received", "send"]}
        }
    },
    {
        "$lookup": {
            "from": "msg_classification_logs",
            "localField": "metadata.id",
            "foreignField": "_id",
            "as": "classification_logs"
        }
    },
    {
        "$project": {
            "msgId": "$metadata.id",
            "text": "$metadata.text",
            "msgDate": "$metadata.timestamp",
            "contactId": "$metadata.contactId",
            "classificationAi": u"$metadata.msg-context.result",
            "classificationFixed": "$classification_logs.newContext",
            "name":"$name",
            "date":"$date"
        }
    }
]

categories = ['water', 'sleep', 'stress', 'workout', 'food', 'none', 'energy']

def select_ai_category(item):
    for category in categories:
        if f'{category}_text' in item:
            return category
    return "empty"

def select_ai_rank(item):
    for category in categories:
        if f'{category}_rank' in item:
            return item[f'{category}_rank']

def select_fixed_category(item):
    if item:
        if item[0]:
            return list(item[0].keys())[0]
        else:
            return "none"
    else:
        return "empty"

def select_fixed_rank(item):

    if item and item[0]:
        category = list(item[0].keys())[0]
        return int(item[0][category]["rank"])

    return None

def combine_fixed_and_ai(item):
    fixed_category = item['fixed_category']
    fixed_rank = item['fixed_rank']
    if fixed_category:
        return fixed_category, fixed_rank
    return item['ai_category'], item['ai_rank']

items = collection.aggregate(pipeline)
df = pd.DataFrame(items)

df['classificationAi'] = df['classificationAi'].apply(lambda x: {} if isinstance(x,float) or not x else x)
df['ai_category'] = df['classificationAi'].apply(select_ai_category)
df['ai_rank'] = df['classificationAi'].apply(select_ai_rank)
df['fixed_category'] = df['classificationFixed'].apply(select_fixed_category)
df['fixed_rank'] = df['classificationFixed'].apply(select_fixed_rank).fillna(-1).apply(int)

df['category'] = df.apply(combine_fixed_and_ai,axis=1).apply(lambda x: x[0])
df['rank'] = df.apply(combine_fixed_and_ai,axis=1).apply(lambda x: x[1])

selected = config['selected']

df[selected].to_pickle(config['save_path']  + f'{today.year}_{today.month}_{today.day}.pkl')