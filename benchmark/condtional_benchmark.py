import json
import pandas as pd
import datetime
#
from itertools import islice, chain
#
def timestamp_to_datetime(timestamp):
    year = timestamp.year
    month = timestamp.month
    day = timestamp.day
    hour = timestamp.hour
    minute = timestamp.minute
    second = timestamp.second

    return datetime.datetime(year=year, month = month, day = day, hour = hour, minute = minute, second = second)


def window(seq, n=2):
    "Returns a sliding window (of width n) over data from the iterable"
    "   s -> (s0,s1,...s[n-1]), (s1,s2,...,sn), ...                   "
    it = iter(seq)
    result = tuple(islice(it, n))
    if len(result) == n:
        yield result
    for elem in it:
        result = result[1:] + (elem,)
        yield result

def select_dialogs_history(df, fields, sort_field, slice = 10):
    df = df.sort_values(sort_field)
    items = df[fields].apply(dict, axis=1).tolist()

    for item in items:
        item['date'] = timestamp_to_datetime(item['date'])

    if slice:
        dialogs = window(items, n = slice)
        return list(dialogs)
    else:
        return [items]


with open('configs/benchmark.config','r') as file:
    config = json.load(file)

df = pd.read_pickle(config['load_path'])

fields = config['save_fields']
sort_field = config['sort_field']

selected = df.groupby('contactId').apply(lambda df : select_dialogs_history(df,
                                                                            fields,
                                                                            sort_field,
                                                                            slice=config['slice']))
dialogs = list(chain(*selected.tolist()))

print(config['save_path'])

pd.to_pickle(dialogs,config['save_path'])