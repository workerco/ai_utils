class Scorer:
    def __init__(self, dialogs):
        self.dataset = dialogs
    def score(self,marks):
        return self._score(marks)
    def _score(self,marks):
        raise NotImplementedError

class CategoryScorer(Scorer):
    def _score(self,marks):
        for dialog,mark in zip(self.dialogs,marks):
            pass