import pymongo
import os
import pandas as pd
import datetime
import json

today = datetime.datetime.today()

with open('../config/clicks.config', 'r') as config:
    config = json.load(config)

url = config['url']
client = pymongo.MongoClient(url)
database = client["heroku_tf7m6qpv"]
collection = database["event_trackers"]

clicks = [item for item in collection.find({
    "name":"click",
    "section":"reply_suggestion"
})]

save_name = f'clicks_{config["name"]}_{today.year}_{today.month}_{today.day}.pkl'
save_path = os.path.join(config['save_folder'], save_name)

pd.to_pickle(clicks, save_path)