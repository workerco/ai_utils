from pymongo import MongoClient
import datetime
import pandas as pd
import json
import tqdm
import os

with open('configs/classifier_error.json','r') as file:
    config = json.load(file)

client = MongoClient(config['url'])

delta = datetime.timedelta(config['delta'])
start = datetime.datetime.today() - delta
end = datetime.datetime.today() + datetime.timedelta(1)

database = client['heroku_tf7m6qpv']

events = database['msg_classification_logs'].find({
    'message.time':{'$gt':start}
})

df = pd.DataFrame(events)

df['text'] = df['message'].apply(lambda context:context['text'])

selected_columns = [
    'text',
    'predicted_category',
    'predicted_rank',
    'category',
    'rank',
    'wrong_category',
    'wrong_rank'
]

def groupby_mistakes(df, selected_columns):

    grouped = df.groupby(['text','category','rank'])

    dfs = []

    for item in tqdm.tqdm(grouped):
        item_df = item[1][selected_columns].copy()
        item_df['freq'] = item[1][selected_columns].shape[0]

        dfs.append(item_df)

    df = pd.concat(dfs).reset_index(drop=True)

    return df

def rank_selector(row, field, key):
    try:
        if isinstance(row[field], dict):
            return row[field][row[key]]['rank']
        return "None"
    except:
        return "Error"


category_selector = lambda marking: list(marking.keys())[0] if marking else 'none'

df['text'] = df['text'].str.strip().str.lower()
df['predicted_category'] = df['oldContext'].apply(category_selector)
df['predicted_rank'] = df.apply(lambda row: rank_selector(row, "oldContext", 'predicted_category'), axis = 1)

df['category'] = df['newContext'].apply(category_selector)
df['rank'] = df.apply(lambda row: rank_selector(row, "newContext", "category"), axis = 1)

df['wrong_category'] = df['predicted_category'] != df['category']
df['wrong_rank'] = df['predicted_rank'] != df['rank']

grouped_mistakes = groupby_mistakes(df, selected_columns)

grouped_mistakes = grouped_mistakes[~grouped_mistakes.duplicated()].copy()
grouped_mistakes = grouped_mistakes.sort_values('freq',ascending=False).reset_index(drop=True)
grouped_mistakes['prob'] = grouped_mistakes['freq']/grouped_mistakes['freq'].sum()

print(grouped_mistakes.head().to_string())
print(grouped_mistakes.query("category == 'none'").sort_values('freq', ascending=False).head().to_string())

save_path = os.path.join(config['save_folder'], f"error_dump_{start.day}_{start.month}_{start.year}_delta_{config['delta']}.pkl")
print(save_path)
grouped_mistakes.to_pickle(save_path)