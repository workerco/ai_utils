from pymongo import MongoClient
import matplotlib.pyplot as plt
import datetime
import pandas as pd
import json
import tqdm
import itertools
import numpy as np
import itertools
import os

from sklearn.metrics import confusion_matrix
from collections import Counter

def plot_confusion_matrix(cm, labels, path, title = None):
    if title:
        plt.title(title)
    plt.imshow(cm, interpolation='nearest')
    tick_marks = np.arange(len(labels))
    plt.xticks(tick_marks, labels, rotation=45)
    plt.yticks(tick_marks, labels)
    thresh = cm.max() / 2
    for i, j in itertools.product(range(cm.shape[0]), range(cm.shape[1])):
        plt.text(j, i, "{:,}".format(cm[i, j]),
                         horizontalalignment="center",
                         color="white" if cm[i, j] > thresh else "black")

    cm_file_name = f'CF_{title}.png'
    cm_save_path = os.path.join(path, cm_file_name)
    plt.savefig(cm_save_path)
    plt.clf()

with open('configs/error_report.json', 'r') as file:
    config = json.load(file)

path = os.path.join(config['error_dump']['folder'], config['error_dump']['file'])

dump_name = config['error_dump']['file'].split('.')[0]
save_folder = os.path.join(config['report']['folder'], dump_name)

os.makedirs(save_folder, exist_ok=True)

dump = pd.read_pickle(path)

selected_indexes = ['predicted_category', 'category', 'freq', 'prob']
cm = dump[selected_indexes].groupby(['predicted_category', 'category']).aggregate('sum').reset_index()

labels = cm['category'].unique()

cm_freq = confusion_matrix(cm['category'], cm['predicted_category'], sample_weight=cm['freq'], labels=labels)
cm_prob = confusion_matrix(cm['category'], cm['predicted_category'], sample_weight=cm['prob'], labels=labels)
cm_percentage = (100*cm_prob).round()

plot_confusion_matrix(cm_freq, labels, save_folder, title = 'Frequency')
plot_confusion_matrix(cm_percentage, labels, save_folder, title = 'Percentage')

rank_errors = cm.query('''predicted_category == category''')['prob'].sum()
category_errors = cm.query('''predicted_category != category''')['prob'].sum()

top_cells_cm = 7
print(cm.sort_values('prob', ascending=False).head(top_cells_cm))

print(f"Rank errors {rank_errors}")
print(f"Category errors {category_errors}")
